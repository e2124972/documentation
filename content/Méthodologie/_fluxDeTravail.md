---
title: "Le flux de travail"
weight: 5
date: 2022-01-11T14:19:27Z
draft: true
---

Pendant les sprints, vous aurez à répondre à 3 types de demandes:
* Développer une fonctionnalité
* Régler un irritant (au niveau du UX)
* Régler un bug

Les 2 premiers types de demande sont décrites par un récit utilisateur. Le troisième est une carte qui réfère à la documentation d'un bug.  

### Développement d’un récit utilisateur (fonctionnalité)  
  
1. Réaliser la maquette  
2. Faire approuver la maquette par le PO, ou le client  
3. Déterminer le dialogue entre le client et le serveur  
   1. Format des requêtes  
   2. Format des réponses de succès  
   3. Format des réponses d’échecs  
4. Coder côté serveur  
   1. Créer une branche pour la fonctionnalité
   2. Coder les tests unitaires  
   3. Coder la fonctionnalité et la tester 
   4. Faire un Merge request de la branche vers la branche dev 
   5. Faire Revue de code  
   6. Intégrer le code de la branche à la branche dev  
5. Coder côté client  
   1. Créer une branche pour la fonctionnalité
   2. Coder la fonctionnalité  
   3. Faire un Merge request de la branche vers la branche dev 
   4. Faire Revue de code  
   5. Intégrer le code de la branche à la branche dev  
